a fork of the golang's buffered io (bufio) package with additional APIs to avoid copies and allow reuse of buffers.
